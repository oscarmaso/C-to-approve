#include <stdio.h>
#include <stdlib.h>


void condicional1(int num1){
	printf("\n Valor de num1: %d",num1);
	
	if(num1 % 2 == 0){
		printf("\n Es Par \n");
	}
	else
		printf("\n Es impar \n");
}
void condicional2(int num1){
	printf("\n Valor de num1: %d",num1);
	
	if(num1 > 1){
		printf("\n Es Positivo");
	}
	else
		printf("\n Es Negativo");
}

int main(){
	
	int num1;
	
	printf("\n Dime el valor del primer numero: ");
	scanf(" %d",&num1);
	
	condicional1(num1);
	condicional2(num1);

	return 0;
}



