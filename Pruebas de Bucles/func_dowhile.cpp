#include <stdio.h>
#include <stdlib.h>

#define exit 1000

int main(){
	
	int num1;
	int num2;
	int num3;
	int contador = 0;
	
	printf("Digite un primer numero: \n");
	scanf(" %d",&num1);	
	printf("Digite un segundo numero: \n");
	scanf(" %d",&num2);	
	
	do
	{
		contador++;
		num2++;
		num1++;
		num3 = num1*num2;
		} while(num3 < exit);
		
	printf(" \n El numero Primero: %d \n",num1);
	printf(" \n El numero Segundo: %d \n",num2);
	printf(" \n El numero Tercero: %d \n",num3);
	printf(" \n Vueltas Dadas: %d \n",contador);
	return 0;
}
