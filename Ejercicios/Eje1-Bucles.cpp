#include <stdio.h>
#include <stdlib.h>

#define N 3
/* Usando bucles :
	
	- Imprime por pantalla 
	                        Valores que tiene que dar el FOR					Valores que tienen

	    																		[ Lugar | Espacios | Asteriscos |
	00*						[	0	|	0	|	*	|							[	0	|	0,1	   |	 2      |
	0**						[	0	|	*	|	*	|							[	1	|	 0 	   |	1,2     |
	*** 					[	*	|	*	|	*	|							[	2	|	Null   |	0,1,2   |
	
	- Siendo 0 Huecos en blanco 	
		 Lugar      = l
		 Espacios   = e
		 Asteriscos = a
*/

int main(){
	
	for(int l=0;l<N;l++){ 												// Bucle de Numeros [L-3]
		for(int e = 0;e<N-l-1;e++){												// Bucle de Numeros [e < 3-L-1]
			printf(" ");	
		}
		for(int a = 0;a<=l;a++){												// Bucle de Numeros [a <=l]
			printf("*");
		}
	printf("\n");
	}
	return 0;			
}
