#include <stdio.h>
#include <stdlib.h>

/* Usando bucles :
	
	- Imprime por pantalla 
	                        Valores que tiene que dar el FOR									Valores que tienen

	    																						[ Lugar | Espacios | 	Asteriscos    |
	00*						[	0	|	0	|	*	|											[	0	|	0,1	   |		 2        |
	0***					[	0	|	*	|	*	|											[	1	|	 0 	   |		1,2       |
	*****					[	*	|	*	|	*	|	*	|	*	|							[	2	|	Null   |	 0,1,2,3,4    |
	0***					[	0	|	*	|	*	|											[	1	|	 0 	   |		1,2       |
	00*						[	0	|	0	|	*	|											[	0	|	0,1	   |		 2        |
	- Siendo 0 Huecos en blanco 	
		 Lugar = l
		 Espacios = e
		 Asteriscos = a
*/

 int main()
 {
	int a, n, n1;
	 int contador = 1;
 
	 for (a = 5; a >= 1; a--){m
		 for (n = 0; n <= a; n++){
			 printf(" ");
		 }
		 for (n1 = 1; n1 <= contador; n1++){
			 printf("*");
		 }
		 printf("\n");                         
		 contador += 2;           
	 }
	 for (a = 4; a >= 1; a--){     
		 for (n = 6; n >= a; n--){        
			 printf(" ");
		 }
		 for (n1 = 2; n1 <= contador-3; n1 ++){
			 printf("*");
		 }
		 printf("\n");
		 contador -= 2;
	 }
	 return 0;
 
	 }
