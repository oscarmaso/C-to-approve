#include <stdio.h>
#include <stdlib.h>

#define N 7

int main(){

	char cadena[N]={'a','b','b','a','a','a','b'};
    char datos[N]={'a','b','c','d','e','f'};
    int contador[N];
    int record = 0;
    
    for(int c=0;c<N;c++)
        contador[c]=0;

    for(int i=0;i<N;i++)
      for(int f=0;f<N;f++)
          if(cadena[i]==datos[f])
            contador[f]++;

    for(int x=0;x<N;x++)
        printf("\n %c Contador %d \n",'a'+x,contador[x]);
}
