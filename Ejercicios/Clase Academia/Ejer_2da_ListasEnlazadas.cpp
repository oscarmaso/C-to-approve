#include <stdio.h>
#include <stdlib.h>
// Mirar Listas Enlazadas
struct _agenda{
    char nombre[20];
    char telefono[12];
    struct _agenda *siguiente;
};
struct _agenda *primero, *ultimo;

void mostrar_menu(){
    printf("\n\nMenu:\n===========\n\n");
    printf("1.- Anadir elementos\n");
    printf("2.- Borrar elementos\n");
    printf("3.- Mostrar elementos\n");
    printf("4.- Salir\n");
    printf("Escoger una opcion:\n");
    fflush(stdout);
}

void anadir_elemento(){
    struct _agenda *nuevo;

    nuevo = (struct _agenda *) malloc (sizeof(struct _agenda));
    if (nuevo==NULL) printf("No hay memoria disponible \n");

    printf("Nuevo elemento: \n");
    printf("Nombre \n"); fflush(stdout);
    scanf(" %s",nuevo->nombre);
    printf("Telefono \n");fflush(stdout);
    scanf(" %s",nuevo->telefono);

    nuevo->siguiente = NULL;

    if(primero==NULL){
        printf("Primer Elemento \n");
        primero = nuevo;
        ultimo = nuevo;
    }
    else{
        ultimo->siguiente = nuevo;
        ultimo = nuevo;
    }
}
void borrado_elemento(){
    struct _agenda *auxiliar;
    auxiliar = primero;
    primero=primero->siguiente;
    free(auxiliar);

}
void mostrar_lista(){
    struct _agenda *auxiliar;
    int i;
    i=0;

    auxiliar = primero;
    printf("\n Mostrar la lista completa \n");
    while(auxiliar!=NULL){
        printf("Nombre: %s, Telefono: %s \n",
                auxiliar->nombre,auxiliar->telefono);
        auxiliar=auxiliar->siguiente;
        i++;
    }
    if (i==0) printf("\n La lista esta vacia!");

}
int main(){
    int opcion;
    primero = (struct _agenda *) NULL;
    ultimo = (struct _agenda *) NULL;
    do{
        mostrar_menu();
        scanf("%d",&opcion);
        switch(opcion) {
            case 1: anadir_elemento();
                    break;
            case 2: borrado_elemento();
                    break;
            case 3: mostrar_lista();
                    break;
            case 4: printf("Saliendo del problema: \n");
                    break;
            default: printf("Opcion no valida \n");
                     break;
        }
    } while(opcion!='4');
    return EXIT_SUCCESS;
}
