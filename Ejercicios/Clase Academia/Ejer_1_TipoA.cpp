#include <stdio.h>
#include <stdlib.h>

#define N 6

int main(){
	
     char cadena[N]={'a','a','c','d','e','f'};
     char a_buscar = 'a';
     int contador = 0;
     
	for(int i=0;i<N;i++){
        printf("%d Valor de el Array: %c \n",i,cadena[i]);
        if(cadena[i] == a_buscar)
            contador++;
    }
    printf("Numero de repeticiones: %d \n",contador);
    
    return 0;
}
