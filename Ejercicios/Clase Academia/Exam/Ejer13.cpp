#include <stdio.h>
#include <stdlib.h>

#define N 200

int main(){
    char frase[N]="Esta frase mola";
    int i=0;
    int contador=0;
    char spa=32;

    while(frase[i]!='\0'){
        if(frase[i]==spa)
            contador++;
        i++;
    }
    printf("Frase: %s \n",frase);
    printf("Contador: %d \n",contador+1);
    return EXIT_SUCCESS;
}
