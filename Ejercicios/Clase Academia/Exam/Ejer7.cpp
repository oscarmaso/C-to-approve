#include <stdio.h>
#include <stdlib.h>

void mostrar_menu(){
    printf("\n Menu: \n============\n");
    printf("1. Sumar \n");
    printf("2. Restar \n");
    printf("3. Multiplicar \n");
    printf("4. Dividir \n");
}

void sumar(double num1,double num2){
    double res= 0;
    res=num1+num2;
    printf("\n Resultado= %2.lf",res);
}

void restar(double num1,double num2){
    double res= 0;
    res=num1-num2;
    printf("\n Resultado= %2.lf",res);
}

void multiplicar(double num1, double num2){
    double res= 0;
    res=num1*num2;
    printf("\n Resultado= %2.lf",res);
}

void dividir(double num1,double num2){
    double res= 0;
    res=num1/num2;
    printf("\n Resultado= %.2f",res);
}

void pedir_datos(){

    double num1=0;
    double num2=0;
    int opcion=0;

    printf("\n Dime el Dato 1: \n");
    scanf(" %lf",&num1);
    printf("\n Dime el Dato 2: \n");
    scanf(" %lf",&num2);

    do{
        mostrar_menu();
        scanf("%d",&opcion);
        switch(opcion){
            case 1: sumar(num1,num2);
                    pedir_datos();
                    break;
            case 2: restar(num1,num2);
                    pedir_datos();
                    break;
            case 3: multiplicar(num1,num2);
                    pedir_datos();
                    break;
            case 4: dividir(num1,num2);
                    pedir_datos();
                    break;

            default: printf("No valido \n");
                     break;
        }
    } while(opcion!=4);
    printf("\n Eliga la opcion Correctamente \n");
}

int main(){

    pedir_datos();

    return EXIT_SUCCESS;
}
