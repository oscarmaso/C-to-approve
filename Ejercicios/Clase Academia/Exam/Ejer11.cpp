#include <stdio.h>
#include <stdlib.h>

#define N 3
// Probar con aux
void transpuesta(int matriz[N][N]){

    int matriz1[N][N];
    int fil = 0;
    int col = 0;

    for (int f=0;f<N;f++){
        for(int c=0;c<N;c++){
            matriz1[f][c] = matriz[fil][col];
            fil++;
        }
        col++;
        fil=0;
    }

    for (int f=0;f<N;f++){
        for(int c=0;c<N;c++)
            printf("%d,",matriz1[f][c]);
        printf("\n");
    }
}
int main(){

    int matriz[N][N];
    int num=1;

    for (int f=0;f<N;f++){
        for(int c=0;c<N;c++){
            matriz[f][c]= num;
            num++;
        }
    }

    for (int f=0;f<N;f++){
        for(int c=0;c<N;c++)
            printf("%d,",matriz[f][c]);
        printf("\n");
    }
    printf("\n\n\n");
    transpuesta(matriz);
    return EXIT_SUCCESS;
}
