#include <stdio.h>
#include <stdlib.h>

// Recursividad
int factorial(int numero,int divisor){

    if(numero==1){
        return 0;
    }
    if(numero%divisor == 0){
        numero=numero/divisor;
        printf("Divisor: %d \n",divisor);
        printf("Numero: %d \n",numero);
        return factorial(numero,divisor);
    }
    return factorial(numero,divisor+1);
}

int main(){

    int numero;
    int divisor=2;

    printf("Dime un Numero: \n");
    scanf(" %d",&numero);

        factorial(numero,divisor);

    return EXIT_SUCCESS;
}
